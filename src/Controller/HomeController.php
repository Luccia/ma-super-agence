<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use Twig\Environment;

class HomeController extends AbstractController{
    // /**
    //  * 
    //  * @var Environment
    //  */
    // public function __construct(Environment $twig){
    //     $this->twig = $twig;
    // }
    //la méthode render permet de traiter directement le twig
    /**
     * @Route("/", name = "home")
     * @return Response
     * @throws \Twig Error Loader
     * @throws \Twig Error Runtime
     * @throws \Twig Error Syntax
     * 
     */
    public function index(): Response{

        //return new Response($this->twig->render('pages/home.html.twig'));
        return $this->render('pages/home.html.twig');
    }
}
